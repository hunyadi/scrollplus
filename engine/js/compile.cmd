@echo off
java -jar closure-compiler-v20180101.jar --compilation_level ADVANCED_OPTIMIZATIONS --isolation_mode IIFE --js scrollplus.js --js_output_file scrollplus.min.js --language_in ECMASCRIPT6_STRICT --language_out ECMASCRIPT5_STRICT --warning_level VERBOSE --jscomp_error=newCheckTypes
