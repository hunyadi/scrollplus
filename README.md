# scrollplus #

scrollplus is a custom scrollbar solution inspired by the standard MacOS scrollbar.

Features:

* Designed for modern browsers, including Chrome, Edge, Firefox and Safari.
* Scrolls vertically or horizontally.
* Exposes all behavior normally associated with scrollable content (uses a solution based on CSS `overflow-x` and `overflow-y`).
* Simple API and out-of-the-box snippets how to get it working on a website.
* 100% CSS and pure JavaScript. No dependencies on third-party JavaScript libraries.
* Small network footprint.

### Examples ###

* [Live demo](http://hunyadi.info.hu/projects/scrollplus/)
